package vn.tcx.crm.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UploadImageResponse {

	private String imageName;
	
	private String fileDownloadUri;
	
//	private boolean imageMain;
//	
//	private boolean imageStatus;
    
//	private String fileType;
//    
//	private long size;
}
