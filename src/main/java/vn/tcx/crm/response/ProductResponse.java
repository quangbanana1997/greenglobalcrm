package vn.tcx.crm.response;

import org.springframework.http.HttpStatus;

import lombok.Data;

@Data
public class ProductResponse {

	private String message;
	HttpStatus status;
}
