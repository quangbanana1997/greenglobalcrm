package vn.tcx.crm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import vn.tcx.crm.entity.Order;

@Repository
public interface OrderRepository extends JpaRepository<Order, Integer>{

//	@Query("SELECT o FROM Order o WHERE o.user.id=?1")
//	Page<Order> findByUserId(int userid, Pageable pageable);
//	
//	@Query("SELECT count(o.id) FROM Order o WHERE o.user.id=?1")
//	long countByUserId(int userId);
}
