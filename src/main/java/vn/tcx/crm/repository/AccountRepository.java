package vn.tcx.crm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import vn.tcx.crm.entity.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account, Integer>{

//	Account findByUsernameAndPassword(String username, String password);
	
//	List<User> findByNameContaining(String s);
}
