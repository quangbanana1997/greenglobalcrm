package vn.tcx.crm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import vn.tcx.crm.entity.Address;

@Repository
public interface AddressRepository extends JpaRepository<Address, Integer>{

//	List<Address> findByUser(User user);
}
