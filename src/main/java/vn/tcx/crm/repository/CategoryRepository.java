package vn.tcx.crm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import vn.tcx.crm.entity.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer>{

}
