package vn.tcx.crm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import vn.tcx.crm.entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer>{
	
	@Query("SELECT p FROM Product p WHERE p.category= :category")
	long countByProductCategory(@Param("category") Product category);
	
	@Query("SELECT p FROM Product p WHERE p.productName LIKE '%' || : productName || '%'")
	List<Product> searchProductByProductName(@Param("productName") String productName);

	@Query(value = "SELECT * FROM product WHERE status = :status and featured = :featured ORDER BY id DESC LIMIT :index", nativeQuery = true)
	List<Product> lastestProducts(@Param("status") boolean productStatus, @Param("featured") boolean productFeatured, @Param("index") int index);
	
	@Query(value = "SELECT * FROM product WHERE status = :status ORDER BY id DESC LIMIT :index", nativeQuery = true)
	List<Product> featuredProducts(@Param("status") boolean productStatus, @Param("index") int index);
	
	@Query(value = "SELECT * FROM product WHERE category_id = :category_id", nativeQuery = true)
	List<Product> findByCategoryId(@Param("category_id") int categoryId);
	
	@Query(value = "SELECT * FROM product WHERE manufacturer_id = :manufacturer_id", nativeQuery = true)
	List<Product> findByManufacturerId(@Param("manufacturer_id") int manufacturerId);
}