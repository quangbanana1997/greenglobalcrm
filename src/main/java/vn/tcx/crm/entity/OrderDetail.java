package vn.tcx.crm.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class OrderDetail implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8214867987258771227L;

	@Id
	@Column(name = "id", nullable = false)
	private String ordId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "order_id", nullable = false)
	private Order order;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "product_id", nullable = false)
	private Product product;
	
	@Column(name = "quantity")
	private int ordQuantity;
	
	@Column(name = "price")
	private double ordPrice;
	
	@Column(name = "amount")
	private double ordAmount;
}
