package vn.tcx.crm.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "`Order`")
public class Order implements Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -4913821195625444190L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int orderId;
	
	@Column(name = "datetime", nullable = false)
	private Timestamp orderDatetime;
	
	@Column(name = "num", nullable = false)
	private double orderNum;
	
	@Column(name = "amount", nullable = false)
	private int orderAmount;
	
	@ManyToOne
	@JoinColumn(name = "account_id", nullable = false)
	private Account orderAccount;
	
	@Column(name = "status", nullable = false)
	private int orderStatus;
}
