package vn.tcx.crm.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Address {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int addressId;
	
	@Column(name = "name", length = 50, nullable = false) 
	private String addressName;
	
	@Column(name = "phone", length = 20, nullable = false) 
	private String addressPhone;
	 
	@Column(name = "province", length = 50, nullable = false)
	private String addressProvince;

	@Column(name = "district", length = 50, nullable = false)
	private String addressDistrict;
	
	@Column(name = "ward", length = 50, nullable = false)
	private String addressWard;
	
	@Column(name = "number", length = 100, nullable = false)
	private String addressNumber;
	
	@Column(name = "type", length = 50)
	private String addressType;
	
	@ManyToOne
	@JoinColumn(name = "account_id", nullable = false)
	private Account addressAccount;
}
