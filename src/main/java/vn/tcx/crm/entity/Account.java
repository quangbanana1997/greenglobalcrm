package vn.tcx.crm.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Account implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4347349245089429058L;
	
	public static final String ROLE_MANAGER = "MANAGER";
    public static final String ROLE_EMPLOYEE = "EMPLOYEE";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false)
	private int accountId;
	
	@Column(name = "username", length = 50, nullable = false)
	private String username;
	
	@Column(name = "password", length = 50, nullable = false)
	private String password;
	
	@Column(name = "full_name", length = 50, nullable = false)
	private String accountFullName;
	
	@Column(name = "sex", length = 10, nullable = false)
	private String accountSex;
	
	@Column(name = "phone", length = 20, nullable = false)
	private String accountPhone;
	
	@Column(name = "email", length = 50, nullable = false)
	private String accountEmail;
	
	@Column(name = "birthday", length = 50)
	private String accountBirthday;
	
	@Column(name = "active", nullable = false)
    private boolean active;
	
	@Column(name = "role", length = 20, nullable = false)
	private String accountRole;
	
	@OneToMany(mappedBy = "addressAccount", cascade = CascadeType.ALL)
	private List<Address> listAccountAddress;
	
}
