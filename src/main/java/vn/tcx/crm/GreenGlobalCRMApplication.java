package vn.tcx.crm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import vn.tcx.crm.properties.FileStorageProperties;

@SpringBootApplication
@EnableConfigurationProperties({FileStorageProperties.class})
public class GreenGlobalCRMApplication {

	public static void main(String[] args) {
		SpringApplication.run(GreenGlobalCRMApplication.class, args);
	}

}
