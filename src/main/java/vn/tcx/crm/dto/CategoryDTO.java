package vn.tcx.crm.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CategoryDTO {

	  private String categoryName;
	  
	  private List<ProductDTO> productDTO;

}
