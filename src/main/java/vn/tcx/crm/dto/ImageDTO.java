package vn.tcx.crm.dto;

import lombok.Getter;
import lombok.Setter;
import vn.tcx.crm.entity.Product;

@Getter
@Setter
public class ImageDTO {

	private String imageName;
	
	private boolean imageMain;
	
	private boolean imageStatus;
	
	private Product product;
	
}
