package vn.tcx.crm.excel;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.enterprisemath.utils.ValidationUtils;


/**
 * Excel report builder.
 *
 * @author radek.hecl
 */
public class XlsxBuilder {
	
    public static final String MESSAGE_BEFORE_INSERT_COL = "must be called before inserting columns";
    public static final String FORMAT_DATA_INT = "#,##0";

    /**
     * Underlined workbook.
     */
    private Workbook workbook;
    /**
     * Current sheet.
     */
    private Sheet sheet = null;
    /**
     * Current row.
     */
    private Row row = null;
    /**
     * Next row index.
     */
    private int nextRowIdx = 0;
    /**
     * Style attributes for row.
     */
    private Set<StyleAttribute> rowStyleAttributes;
    /**
     * Next column index.
     */
    private int nextColumnIdx = 0;
    private Map<Set<StyleAttribute>, CellStyle> styleBank = new HashMap<>();

    /**
     * Creates new instance.
     */
    public XlsxBuilder() {
        workbook = new XSSFWorkbook();
    }

    /**
     * Starts sheet.
     *
     * @param name sheet name
     * @return this instance
     */
    public XlsxBuilder startSheet(String name) {
        sheet = workbook.createSheet(name);
        nextRowIdx = 0;
        nextColumnIdx = 0;
        rowStyleAttributes = new HashSet<>();
        return this;
    }

    /**
     * Sets auto sizing columns.
     *
     * @param idx column index, starting from 0
     * @return this instance
     */
    public XlsxBuilder setAutoSizeColumn(int idx) {
        sheet.autoSizeColumn(idx);
        return this;
    }

    /**
     * Sets column size.
     *
     * @param idx column index, starting from 0
     * @param m   number of 'M' standard characters to use for size calculation
     * @return this instance
     */
    public XlsxBuilder setColumnSize(int idx, int m) {
        sheet.setColumnWidth(idx, (m + 1) * 256);
        return this;
    }

    /**
     * Starts new row.
     *
     * @return this instance
     */
    public XlsxBuilder startRow() {
        row = sheet.createRow(nextRowIdx);
        nextRowIdx = nextRowIdx + 1;
        nextColumnIdx = 0;
        rowStyleAttributes = new HashSet<>();
        return this;
    }

    /**
     * Sets row top border as thin.
     *
     * @return this instance
     */
    public XlsxBuilder setRowThinTopBorder() {
        ValidationUtils.guardEquals(0, nextColumnIdx, MESSAGE_BEFORE_INSERT_COL);
        row.setRowStyle(getCellStyle(StyleAttribute.THIN_TOP_BORDER));
        rowStyleAttributes.add(StyleAttribute.THIN_TOP_BORDER);
        return this;
    }

    /**
     * Sets row top border as thick.
     *
     * @return this instance
     */
    public XlsxBuilder setRowThickTopBorder() {
        ValidationUtils.guardEquals(0, nextColumnIdx, MESSAGE_BEFORE_INSERT_COL);
        row.setRowStyle(getCellStyle(StyleAttribute.THICK_TOP_BORDER));
        rowStyleAttributes.add(StyleAttribute.THICK_TOP_BORDER);
        return this;
    }

    /**
     * Sets row bottom border as thin.
     *
     * @return this instance
     */
    public XlsxBuilder setRowThinBottomBorder() {
        ValidationUtils.guardEquals(0, nextColumnIdx, MESSAGE_BEFORE_INSERT_COL);
        row.setRowStyle(getCellStyle(StyleAttribute.THIN_BOTTOM_BORDER));
        rowStyleAttributes.add(StyleAttribute.THIN_BOTTOM_BORDER);
        return this;
    }

    /**
     * Sets row bottom border as thick.
     *
     * @return this instance
     */
    public XlsxBuilder setRowThickBottomBorder() {
        ValidationUtils.guardEquals(0, nextColumnIdx, MESSAGE_BEFORE_INSERT_COL);
        row.setRowStyle(getCellStyle(StyleAttribute.THICK_BOTTOM_BORDER));
        rowStyleAttributes.add(StyleAttribute.THICK_BOTTOM_BORDER);
        return this;
    }

    /**
     * Sets row height to capture the title.
     *
     * @return this instance
     */
    public XlsxBuilder setRowTitleHeight() {
        ValidationUtils.guardEquals(0, nextColumnIdx, MESSAGE_BEFORE_INSERT_COL);
        row.setHeightInPoints(30);
        return this;
    }

    /**
     * Adds title column.
     *
     * @param text text
     * @return this instance
     */
    public XlsxBuilder addTitleTextColumn(String text) {
        Cell cell = row.createCell(nextColumnIdx);
        CellStyle style = getCellStyle(StyleAttribute.TITLE_SIZE, StyleAttribute.BOLD);
        cell.setCellStyle(style);
        cell.setCellValue(StringUtils.stripToEmpty(text));
        nextColumnIdx = nextColumnIdx + 1;
        return this;
    }

    /**
     * Adds simple left aligned text.
     *
     * @param text text
     * @return this instance
     */
    public XlsxBuilder addTextLeftAlignedColumn(String text) {
        Cell cell = row.createCell(nextColumnIdx);
        CellStyle style = getCellStyle(StyleAttribute.ALIGN_LEFT);
        cell.setCellStyle(style);
        cell.setCellValue(StringUtils.stripToEmpty(text));
        nextColumnIdx = nextColumnIdx + 1;
        return this;
    }

    /**
     * Adds simple center aligned text.
     *
     * @param text text
     * @return this instance
     */
    public XlsxBuilder addTextCenterAlignedColumn(String text) {
        Cell cell = row.createCell(nextColumnIdx);
        CellStyle style = getCellStyle(StyleAttribute.ALIGN_CENTER);
        cell.setCellStyle(style);
        cell.setCellValue(StringUtils.stripToEmpty(text));
        nextColumnIdx = nextColumnIdx + 1;
        return this;
    }

    /**
     * Adds simple center aligned text.
     *
     * @param val value
     * @return this instance
     */
    public XlsxBuilder addDoubleCenterAlignedColumn(double val) {
        Cell cell = row.createCell(nextColumnIdx);
        CellStyle style = getCellStyle(StyleAttribute.ALIGN_CENTER);
        cell.setCellStyle(style);
        cell.setCellValue(val);
        nextColumnIdx = nextColumnIdx + 1;
        return this;
    }

    /**
     * Adds bold left aligned text.
     *
     * @param text text
     * @return this instance
     */
    public XlsxBuilder addBoldTextLeftAlignedColumn(String text) {
        Cell cell = row.createCell(nextColumnIdx);
        CellStyle style = getCellStyle(StyleAttribute.ALIGN_LEFT, StyleAttribute.BOLD);
        cell.setCellStyle(style);
        cell.setCellValue(StringUtils.stripToEmpty(text));
        nextColumnIdx = nextColumnIdx + 1;
        return this;
    }

    /**
     * Adds bold center aligned text.
     *
     * @param text text
     * @return this instance
     */
    public XlsxBuilder addBoldTextCenterAlignedColumn(String text) {
        Cell cell = row.createCell(nextColumnIdx);
        CellStyle style = getCellStyle(StyleAttribute.ALIGN_CENTER, StyleAttribute.BOLD);
        cell.setCellStyle(style);
        cell.setCellValue(StringUtils.stripToEmpty(text));
        nextColumnIdx = nextColumnIdx + 1;
        return this;
    }

    /**
     * Builds the result object. Object cannot be reused after calling this method.
     *
     * @return created object
     */
    public byte[] build() {
        try(ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
            workbook.write(bos);
            return bos.toByteArray();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        
    }

    /**
     * Returns cell style.
     *
     * @param attrs attributes
     * @return cell style
     */
    @SuppressWarnings("deprecation")
	private CellStyle getCellStyle(StyleAttribute... attrs) {
        Set<StyleAttribute> allattrs = new HashSet<StyleAttribute>();
        allattrs.addAll(rowStyleAttributes);
        allattrs.addAll(Arrays.asList(attrs));
        if (styleBank.containsKey(allattrs)) {
            return styleBank.get(allattrs);
        }
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        style.setFont(font);
        style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
        for (StyleAttribute attr : allattrs) {
            if (attr.equals(StyleAttribute.TITLE_SIZE)) {
                font.setFontHeightInPoints((short) 18);
            } else if (attr.equals(StyleAttribute.BOLD)) {
                font.setBoldweight(Font.BOLDWEIGHT_BOLD);
            } else if (attr.equals(StyleAttribute.THIN_TOP_BORDER)) {
                style.setBorderTop(CellStyle.BORDER_THIN);
            } else if (attr.equals(StyleAttribute.THIN_BOTTOM_BORDER)) {
                style.setBorderBottom(CellStyle.BORDER_THIN);
            } else if (attr.equals(StyleAttribute.THICK_TOP_BORDER)) {
                style.setBorderTop(CellStyle.BORDER_THICK);
            } else if (attr.equals(StyleAttribute.THICK_BOTTOM_BORDER)) {
                style.setBorderBottom(CellStyle.BORDER_THICK);
            } else if (attr.equals(StyleAttribute.ALIGN_LEFT)) {
                style.setAlignment(CellStyle.ALIGN_LEFT);
            } else if (attr.equals(StyleAttribute.ALIGN_CENTER)) {
                style.setAlignment(CellStyle.ALIGN_CENTER);
            } else if (attr.equals(StyleAttribute.ALIGN_RIGHT)) {
                style.setAlignment(CellStyle.ALIGN_RIGHT);
            } else {
                throw new RuntimeException("unknown cell style attribute: " + attr);
            }
        }
        styleBank.put(allattrs, style);
        return style;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    /**
     * Possible style attributes.
     */
    private static enum StyleAttribute {
        /**
         * Thin top border.
         */
        THIN_TOP_BORDER,
        /**
         * Thin bottom border.
         */
        THIN_BOTTOM_BORDER,
        /**
         * Thick top border.
         */
        THICK_TOP_BORDER,
        /**
         * Thick bottom border.
         */
        THICK_BOTTOM_BORDER,
        /**
         * Title font size.
         */
        TITLE_SIZE,
        /**
         * Bold font.
         */
        BOLD,
        /**
         * Left alignment.
         */
        ALIGN_LEFT,
        /**
         * Center alignment.
         */
        ALIGN_CENTER,
        /**
         * Right alignment.
         */
        ALIGN_RIGHT
    }

    public XlsxBuilder setContent(String rptName, List<String> tableHeader, Object[][] data) {
        if (!StringUtils.isEmpty(rptName)) {
            startRow();
            sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, tableHeader.size() - 1));
            addBoldTextCenterAlignedColumn(rptName);
        }

        nextRowIdx = nextRowIdx + 2;
        nextColumnIdx = 0;

        startRow();
        for (String headerName : tableHeader) {
            addBoldTextCenterAlignedColumn(headerName);
        }

        for (Object[] aBook : data) {
            startRow();
            for (Object field : aBook) {
                if (field instanceof String) {
                    addTextLeftAlignedColumn((String) field);
                } else if (field instanceof Integer) {
                	addIntegerRightAlignedColumn((Integer) field);
                } else if (field instanceof Double) {
                    addDoubleRightAlignedColumn((Double) field);
                } else if (field instanceof ZonedDateTime) {
                    addZonedDateTimeCenterAlignedColumn(String.valueOf(
                            DateTimeFormatter.ofPattern("dd/MM/yyyy").format((ZonedDateTime) field)));
                } else {
                    if (field != null) {
                        addTextRightAlignedColumn(String.valueOf(field));
                    }else {
                        addTextRightAlignedColumn("");
                    }
                }
            }
        }

        // auto size column
        for (int j = 0; j < tableHeader.size(); j++) {
            sheet.autoSizeColumn(j);
        }

        return this;
    }

    /**
     * Adds simple right aligned text.
     *
     * @param val value
     * @return this instance
     */
    public void addDoubleRightAlignedColumn(double val) {
        Cell cell = row.createCell(nextColumnIdx);
        CellStyle style = getCellStyle(StyleAttribute.ALIGN_RIGHT);
        DataFormat format = workbook.createDataFormat();
        style.setDataFormat(format.getFormat(FORMAT_DATA_INT));
        cell.setCellStyle(style);
        cell.setCellValue(val);
        nextColumnIdx = nextColumnIdx + 1;
    }

    /**
     * Adds simple right aligned int.
     *
     * @param val value
     * @return this instance
     */
    public void addIntegerRightAlignedColumn(int val) {
        Cell cell = row.createCell(nextColumnIdx);
        CellStyle style = getCellStyle(StyleAttribute.ALIGN_RIGHT);
        DataFormat format = workbook.createDataFormat();
        style.setDataFormat(format.getFormat(FORMAT_DATA_INT));
        cell.setCellStyle(style);
        cell.setCellValue(val);
        nextColumnIdx = nextColumnIdx + 1;
    }

    /**
     * Adds simple center aligned int.
     *
     * @param val value
     * @return this instance
     */
    public void addIntegerCenterAlignedColumn(int val) {
        Cell cell = row.createCell(nextColumnIdx);
        CellStyle style = getCellStyle(StyleAttribute.ALIGN_CENTER);
        DataFormat format = workbook.createDataFormat();
        style.setDataFormat(format.getFormat(FORMAT_DATA_INT));
        cell.setCellStyle(style);
        cell.setCellValue(val);
        nextColumnIdx = nextColumnIdx + 1;
    }

    /**
     * Adds simple right aligned text.
     *
     * @param text text
     * @return this instance
     */
    public void addTextRightAlignedColumn(String text) {
        Cell cell = row.createCell(nextColumnIdx);
        CellStyle style = getCellStyle(StyleAttribute.ALIGN_RIGHT);
        cell.setCellStyle(style);
        cell.setCellValue(StringUtils.stripToEmpty(text));
        nextColumnIdx = nextColumnIdx + 1;
    }

    /**
     * Adds simple right aligned text.
     *
     * @param val value
     * @return this instance
     */
    public void addZonedDateTimeRightAlignedColumn(String dateStr) {
        Cell cell = row.createCell(nextColumnIdx);
        CellStyle style = getCellStyle(StyleAttribute.ALIGN_RIGHT);
        cell.setCellStyle(style);
        cell.setCellValue(dateStr);
        nextColumnIdx = nextColumnIdx + 1;
    }

    /**
     * Adds simple right aligned text.
     *
     * @param val value
     * @return this instance
     */
    public void addZonedDateTimeCenterAlignedColumn(String dateStr) {
        Cell cell = row.createCell(nextColumnIdx);
        CellStyle style = getCellStyle(StyleAttribute.ALIGN_CENTER);
        cell.setCellStyle(style);
        cell.setCellValue(dateStr);
        nextColumnIdx = nextColumnIdx + 1;
    }
}