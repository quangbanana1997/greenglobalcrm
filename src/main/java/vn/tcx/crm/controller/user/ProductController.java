package vn.tcx.crm.controller.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import vn.tcx.crm.dto.ProductImageDTO;
import vn.tcx.crm.entity.Product;
import vn.tcx.crm.service.ProductService;

@Controller
@RequestMapping("/products")
public class ProductController {

	@Autowired
	private ProductService productService;
	
	@GetMapping
	public String listProduct(Model model) {
		model.addAttribute("featuredProducts", productService.featuredProducts(true, 12));
		return "products";
	}
	
	// Chi tiết sản phẩm
	@GetMapping("/details/{productId}")
	public String productDetail(@PathVariable("productId") int productId, Model model) {
		ProductImageDTO productDetail = productService.getByProductImageIdDTO(productId);
		model.addAttribute("productDetail", productDetail);
		model.addAttribute("lastestProducts", productService.lastestProducts(true, true, 6));
		return "product-detail";
	}
	
	@GetMapping("/search")
	public String searchProductByProductName(@RequestParam(value = "search", required = false) String productName, Model model) {
		int result = productService.searchProductByProductName(productName).size();
		if (result == 0) {
			model.addAttribute("message", "Hãy thử các từ khóa khác nhau hoặc cụ thể hơn!");
		} else {
			model.addAttribute("catalogList", productService.searchProductByProductName(productName));
		}
		return "products";
	}
	
	@GetMapping("/women")
	public String catalogWomen(@PathVariable(value = "productCategory") Product productCategory, Model model) {
		model.addAttribute("productWomenList", productService.countByProductCategory(productCategory));
		return "products";
	}
	
	@GetMapping("/men")
	public String catalogMen(@PathVariable(value = "productCategory") Product productCategory, Model model) {
		model.addAttribute("productMenList", productService.countByProductCategory(productCategory));
		return "products";
	}
	
}
