package vn.tcx.crm.controller.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import vn.tcx.crm.service.ProductService;

@Controller
public class HomeController {
	
	@Autowired
	private ProductService productService;
	
	@GetMapping("/")
	public String home(Model model) {
		model.addAttribute("lastestProducts", productService.lastestProducts(true, true, 6));
		model.addAttribute("featuredProducts", productService.featuredProducts(true, 12));
		return "index";
	}
	 
	@GetMapping("/login")
	public String login() {
		return "login";
	}
	
	@GetMapping("/register")
	public String register() {
		return "register";
	}

}
