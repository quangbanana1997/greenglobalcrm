package vn.tcx.crm.controller.user;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import vn.tcx.crm.entity.Item;
import vn.tcx.crm.service.CartService;
import vn.tcx.crm.service.ProductService;

@Controller
public class CartController {

	@Autowired
	private ProductService productService;
	
	@Autowired
	private CartService cartService;
	
	@SuppressWarnings("unchecked")
	@GetMapping("/cart")
	public String cart(Model model, HttpSession session) {
		int countItems = 0;
		double total = 0;
		if(session.getAttribute("cart") != null) {
			List<Item> cart = (List<Item>) session.getAttribute("cart");
			countItems = cart.size();
			for(Item item : cart) {
				total += item.getProduct().getProductPrice() * item.getProductQuantity();
			}
		}
		model.addAttribute("countItems", countItems);
		model.addAttribute("total", total);
		return "cart";
	}
	
	@SuppressWarnings("unchecked")
	@GetMapping("/cart/add/{productId}")
	public String addToCart(@PathVariable(value = "productId") int productId, Model model, HttpSession session) {
		if(session.getAttribute("cart") == null) {
			List<Item> cart = new ArrayList<>();
			cart.add(new Item(productService.getByProductId(productId), 1));
			session.setAttribute("cart", cart);
		} else {
			List<Item> cart = (List<Item>) session.getAttribute("cart");
			int index = cartService.isExisting(productId, cart);
			if (index == -1) {
				cart.add(new Item(productService.getByProductId(productId), 1));
			} else {
				int newQuantity = cart.get(index).getProductQuantity() +1;
				cart.get(index).setProductQuantity(newQuantity);
			}
			session.setAttribute("cart", cart);
		}
		return "redirect:/cart";
	}
	
	@SuppressWarnings("unchecked")
	@GetMapping("/cart/update")
	public String updateItem(@PathVariable(value = "quantities") int[] quantities, Model model, HttpSession session) {
		
		if(session.getAttribute("cart") != null) {
			List<Item> cart = (List<Item>) session.getAttribute("cart");
			
			for(int i = 0; i < cart.size(); i++) {
				cart.get(i).setProductQuantity(quantities[i]);
			}
			session.setAttribute("cart", cart);
		}
		return "redirect:/cart";
	}
	
	@SuppressWarnings("unchecked")
	@GetMapping("/cart/delete/{productId}")
	public String deteleItem(@PathVariable(value = "productId") int productId, Model model, HttpSession session) {
		List<Item> cart = (List<Item>) session.getAttribute("cart");
		int index = cartService.isExisting(productId, cart);
		cart.remove(index);
		session.setAttribute("cart", cart);
		return "redirect:/cart";
	}
}
