package vn.tcx.crm.controller.rest;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import vn.tcx.crm.dto.ProductDTO;
import vn.tcx.crm.entity.Product;
import vn.tcx.crm.exception.NotFoundException;
import vn.tcx.crm.service.CategoryService;
import vn.tcx.crm.service.ManufacturerService;
import vn.tcx.crm.service.ProductService;

@RestController
@RequestMapping(path = "/api/v1/admin")
public class ProductRestController {

	@Autowired
	private ProductService productService;

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private ManufacturerService manufacturerService;

	@GetMapping("/products")
	public List<Product> findAllProducts() {

		return productService.findAllProductAdmin();
	}

	@GetMapping("/categories/{categoryId}/products")
	public List<Product> getAllProductByCategoryId(@PathVariable int categoryId) {

		if (!categoryService.existsCategoryById(categoryId)) {
			throw new NotFoundException("Category not found!");
		}

		return productService.findByCategoryId(categoryId);
	}

	@GetMapping("/manufacturers/{manufacturerId}/products")
	public List<Product> getAllProductByManufacturerId(@PathVariable int manufacturerId) {

		if (!manufacturerService.existsManufacturerById(manufacturerId)) {
			throw new NotFoundException("Manufacturer not found!");
		}

		return productService.findByManufacturerId(manufacturerId);
	}

	@GetMapping("/products/{productId}")
	public Product getProductById(@PathVariable int productId) {

		Optional<Product> product = productService.findProductById(productId);

		if (product.isPresent()) {
			return product.get();
		} else {
			throw new NotFoundException("Product not found with id " + productId);
		}
	}

	@PostMapping("/products")
	public Product createProduct(@RequestBody ProductDTO productDTO) {

		Product product = null;

		try {
			product = productService.createProduct(productDTO);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return product;
	}

//	@RequestParam(required = true) int categoryId,
//	@RequestParam(required = true) int manufacturerId,
	@PutMapping("/products/{productId}")
	public Product updateProduct(@PathVariable int productId, @RequestBody ProductDTO productDTO) {

		Product product = null;

		try {
			product = productService.updateProduct(productId, productDTO);
		} catch (Exception e) {
			throw new NotFoundException(e.getMessage());
		}

		return product;
	}

	@DeleteMapping("/products/{productId}")
	public String deleteProduct(@PathVariable int productId) {

		Product product = productService.findProductById(productId)
				.orElseThrow(() -> new NotFoundException("Product not found with id " + productId));

		productService.deleteProuct(product);

		return "Deleted Successfully!";
	}

	@GetMapping("/products/downloads/excel")
	public ResponseEntity<byte[]> exportExcel() {
		List<Product> products = productService.findAllProductAdmin();
		return productService.exportExcel(products);
	}
	
	@GetMapping("/products/downloads/csv")
	public void exportCSV(HttpServletResponse response) {
		productService.exportCSV(response);
	}
	
	@GetMapping("/products/downloads/word")
	public ResponseEntity<byte[]> exportWord() throws Exception {
		return productService.exportWord();
	}
	
}
