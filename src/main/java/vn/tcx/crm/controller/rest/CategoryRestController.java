package vn.tcx.crm.controller.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import vn.tcx.crm.entity.Category;
import vn.tcx.crm.exception.NotFoundException;
import vn.tcx.crm.service.CategoryService;

@RestController
@RequestMapping("/api/v1/admin/categories")
public class CategoryRestController {

	@Autowired
	private CategoryService categoryService;
	
	@GetMapping
	public List<Category> findAllCategories(){
		
		return categoryService.findAllCategory();
	}
	
	@PostMapping
	public Category createCategory(@RequestBody Category category) {
		
		return categoryService.saveCategory(category);
	}
	
	@GetMapping("/{categoryId}")
	public Category getCategoryById(@PathVariable int categoryId){
		
		Optional<Category> category = categoryService.findCategoryById(categoryId);
		
		if (category.isPresent()) {
			return category.get();
		} else {
			throw new NotFoundException("Category not found with id " + categoryId);
		}
		
	}
	
	@PutMapping("/{categoryId}")
	public Category updateCategory(@PathVariable("categoryId") int categoryId, @RequestBody Category categoryUpdated){
		
		Category category = null;
		
		try {
			category = categoryService.updateCategory(categoryId, categoryUpdated);
		} catch (Exception e) {
			throw new NotFoundException(e.getMessage());
		}
		
		return category;
	}
	
	@DeleteMapping("/{categoryId}")
	public String deleteCategory(@PathVariable int categoryId) {
		
		Category category = categoryService.findCategoryById(categoryId)
				.orElseThrow(() -> new NotFoundException("Category not found with id " + categoryId));
			
		categoryService.deleteCategory(category);
		
		return "Deleted Successfully!";
			
	}
	
}
