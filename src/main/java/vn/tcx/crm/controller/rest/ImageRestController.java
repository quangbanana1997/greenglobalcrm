package vn.tcx.crm.controller.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import vn.tcx.crm.entity.Image;
import vn.tcx.crm.service.ImageService;

@RestController
@RequestMapping("/api/v1/admin/images")
public class ImageRestController {

	@Autowired
	private ImageService imageService;
	
	@GetMapping
	public List<Image> findAllImages(){
		
		return imageService.findAllImages();
	}
	
}
