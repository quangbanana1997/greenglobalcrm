package vn.tcx.crm.controller.rest;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import vn.tcx.crm.entity.Manufacturer;
import vn.tcx.crm.exception.NotFoundException;
import vn.tcx.crm.service.ManufacturerService;

@RestController
@RequestMapping("/api/v1/admin/manufacturers")
public class ManufacturerRestController {

	@Autowired
	private ManufacturerService manufacturerService;
	
	@GetMapping
	public List<Manufacturer> getAllManufacturers(){
		
		return manufacturerService.findAllManufacturer();
	}
	
	@GetMapping("/{manufacturerId}")
	public Manufacturer getManufacturerById(@PathVariable int manufacturerId){
		
		Optional<Manufacturer> manufacturer = manufacturerService.findManufacturerById(manufacturerId);
		
		if (manufacturer.isPresent()) {
			return manufacturer.get();
		} else {
			throw new NotFoundException("Manufacturer not found with id " + manufacturerId);
		}
		
	}
	
	
	@PostMapping
	public Manufacturer createManufacturer(@Valid @RequestBody Manufacturer manufacturer) {
		
		return manufacturerService.saveManufacturer(manufacturer);
	}
	
	@PutMapping("/{manufacturerId}")
	public Manufacturer updateManufacturer(@PathVariable("manufacturerId") int manufacturerId, @Valid @RequestBody Manufacturer manufacturerUpdated){
		
		return manufacturerService.findManufacturerById(manufacturerId).map(manufacturer -> {
			manufacturer.setManufacturerName(manufacturerUpdated.getManufacturerName());
			
			return manufacturerService.saveManufacturer(manufacturer);
			
		}).orElseThrow(() -> new NotFoundException("Manufacturer not found with id " + manufacturerId));
	}
	
	@DeleteMapping("/{manufacturerId}")
	public String deleteManufacturer(@PathVariable int manufacturerId) {
		
		Manufacturer manufacturer = manufacturerService.findManufacturerById(manufacturerId)
				.orElseThrow(() -> new NotFoundException("Manufacturer not found with id " + manufacturerId));
			
		manufacturerService.deleteManufacturer(manufacturer);
		
		return "Deleted Successfully!";
			
	}
}
