package vn.tcx.crm.controller.admin;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import vn.tcx.crm.entity.Category;
import vn.tcx.crm.service.CategoryService;

@Controller
public class CategoryAdminController {
	
	@Autowired
	private CategoryService categoryService;
	
	@GetMapping("/admin/category/list")
	public String listCategory(Model model) {
		
		model.addAttribute("categories", categoryService.findAllCategory());
		
		return "admin/category-list";
	}
	
	@GetMapping("/admin/category/new")
	public String newCategory(Model model) {
		
		model.addAttribute("category", new Category());
		
		return "admin/category-addnew";
	}
	
	@GetMapping("/admin/category/edit/{categoryId}")
	public String editCategory(@PathVariable int categoryId, Model model) {
		
		model.addAttribute("category", categoryService.findCategoryById(categoryId));
		
		return "admin/category-edit";
	}
	
	@GetMapping("/admin/category/delete/{categoryId}")
	public String deleteCategory(@PathVariable int categoryId, RedirectAttributes redirect) {
		
		Category category = categoryService.getCategoryById(categoryId);
		categoryService.deleteCategory(category);
		redirect.addFlashAttribute("success", "Đã xóa Danh mục thành công!");
		
		return "redirect:/admin/category/list";
	}
	
	@PostMapping("/admin/category/new/save")
	public String saveAddCategory(@Valid Category category, BindingResult result, RedirectAttributes redirect) {
		
		if(result.hasErrors()) {
			return "admin/category/new";
		}
		
		categoryService.saveCategory(category);
		redirect.addFlashAttribute("success", "Thêm 1 Danh mục mới thành công!");
		
		return "redirect:/admin/category/list";
	}
	
	@PostMapping("/admin/category/edit/save")
	public String saveEditCategory(@Valid Category category, BindingResult result, RedirectAttributes redirect) {
		
		if(result.hasErrors()) {
			return "admin/category-edit";
		}
		
		categoryService.saveCategory(category);
		redirect.addFlashAttribute("success", "Chỉnh sửa Tài khoản thành công!");
		
		return "redirect:/admin/category/list";
	}
}
