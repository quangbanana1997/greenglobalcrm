package vn.tcx.crm.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import vn.tcx.crm.entity.Manufacturer;
import vn.tcx.crm.service.ManufacturerService;

@Controller
public class ManufacturerAdminController {

	@Autowired
	private ManufacturerService manufacturerService;
	
	@GetMapping("/admin/manufacturer/list")
	public String listManufacturer(Model model) {
		
		model.addAttribute("manufacturers", manufacturerService.findAllManufacturer());
		
		return "admin/manufacturer-list";
	}
	
	@GetMapping("admin/manufacturer/new")
	public String newManufacturer(Model model) {
		
		model.addAttribute("manufacturer", new Manufacturer());
		
		return "admin/manufacturer-addnew";
	}
	
	@GetMapping("/admin/manufacturer/edit/{manufacturerId}")
	public String editManufacturer(@PathVariable int manufacturerId, Model model) {
		
		model.addAttribute("manufacturer", manufacturerService.getManufacturerById(manufacturerId));
		
		return "admin/manufacturer-edit";
	}
	
	@GetMapping("/admin/manufacturer/delete/{manufacturerId}")
	public String deleteManufacturer(@PathVariable int manufacturerId, RedirectAttributes redirect) {
		
		Manufacturer manufacturer = manufacturerService.getManufacturerById(manufacturerId);
		manufacturerService.deleteManufacturer(manufacturer);
		redirect.addFlashAttribute("success", "Đã xóa Thương hiệu thành công!");
		
		return "redirect:/admin/manufacturer/list";
	}
	
	@PostMapping("/admin/manufacturer/new/save")
	public String saveAddManufacturer(@ModelAttribute(value = "manufacturer") Manufacturer manufacturer, BindingResult result, RedirectAttributes redirect) {
		
		if(result.hasErrors()) {
			return "admin/manufacturer/new";
		}
		
		manufacturerService.saveManufacturer(manufacturer);
		redirect.addFlashAttribute("success", "Thêm 1 Thương hiệu mới thành công!");
		
		return "redirect:/admin/manufacturer/list";
	}
	
	@PostMapping("/admin/manufacturer/edit/save")
	public String saveEditCategory(@ModelAttribute(value = "manufacturer") Manufacturer manufacturer, BindingResult result, RedirectAttributes redirect) {
		
		if(result.hasErrors()) {
			return "admin/manufacturer-edit";
		}
		
		manufacturerService.saveManufacturer(manufacturer);
		redirect.addFlashAttribute("success", "Chỉnh sửa Thương hiệu thành công!");
		
		return "redirect:/admin/manufacturer/list";
	}
}
