package vn.tcx.crm.controller.admin;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import lombok.Getter;
import lombok.Setter;
import vn.tcx.crm.entity.Account;
import vn.tcx.crm.service.AccountService;
import vn.tcx.crm.service.AddressService;
import vn.tcx.crm.service.OrderService;

@Controller
@Setter
@Getter
public class AccountAdminController {

	@Autowired 
	private AccountService accountService;
	
	@Autowired
	private OrderService orderService;
	
	@Autowired
	private AddressService addressService;
	
	
	@GetMapping("/admin/account/list")
	public String listAccount(Model model) {
		model.addAttribute("accountList", accountService.getAllAccount());
		return "admin/account-list";
	}
	
	@GetMapping("admin/account/new")
	public String newAccount(Model model) {
		model.addAttribute("account", new Account());
		return "admin/account-addnew";
	}
	
	@GetMapping("/admin/account/edit/{accountId}")
	public String editAccount(@PathVariable int accountId, Model model) {
		model.addAttribute("account", accountService.getAccountId(accountId));
		return "admin/account-edit";
	}
	
	@GetMapping("/admin/account/delete/{accountId}")
	public String deleteAccount(@PathVariable int accountId, RedirectAttributes redirect) {
		Account account = accountService.getAccountId(accountId);
		accountService.deleteAccount(account);
		redirect.addFlashAttribute("success", "Đã xóa Tài khoản thành công!");
		return "redirect:/admin/account-list";
	}
	
	@PostMapping("/admin/account/new/save")
	public String saveAddAccount(@Valid Account account, BindingResult result, RedirectAttributes redirect) {
		if(result.hasErrors()) {
			return "admin/account/new";
		}
		accountService.saveAccount(account);
		redirect.addFlashAttribute("success", "Thêm 1 Tài khoản mới thành công!");
		return "redirect:/admin/account/list";
	}
	
	@PostMapping("/admin/account/edit/save")
	public String saveEditAccount(@Valid Account account, BindingResult result, RedirectAttributes redirect) {
		if(result.hasErrors()) {
			return "admin/account-edit";
		}
		accountService.saveAccount(account);
		redirect.addFlashAttribute("success", "Chỉnh sửa Tài khoản thành công!");
		return "redirect:/admin/account/list";
	}
}
