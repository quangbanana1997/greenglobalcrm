package vn.tcx.crm.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import vn.tcx.crm.entity.Image;
import vn.tcx.crm.entity.Product;
import vn.tcx.crm.service.ImageService;
import vn.tcx.crm.service.ProductService;

@Controller
public class ImageAdminController {

	@Autowired
	private ProductService productService;

	@Autowired
	private ImageService imageService;

	@GetMapping("/admin/product/{productId}/image/list")
	public String listImageProduct(@PathVariable("productId") int productId, Model model) {

		model.addAttribute("images", productService.getByProductId(productId).getImages());

		return "admin/product-image-list";
	}

	@GetMapping("/admin/product/{productId}/image/add")
	public String addImageProduct(@PathVariable("productId") int productId, Model model) {

		Product product = productService.getByProductId(productId);

		Image image = new Image();

		image.setImageStatus(true);
		image.setProduct(product);

		model.addAttribute("image", image);
		model.addAttribute("product", product);

		return "admin/product-image-add";
	}
	
	@PostMapping("/admin/product/image/add/save")
	public String saveAddImageProduct(@ModelAttribute("image") Image image, @RequestParam("file") MultipartFile file) {

		image.setImageName(imageService.storeFile(file));
		
		// Update main of Products
		if (image.isImageMain()) {
			Product product = productService.getByProductId(image.getProduct().getProductId());
			if (product.getImages() != null && !product.getImages().isEmpty()) {
				for (Image i : product.getImages()) {
					i.setImageMain(false);
					imageService.saveImage(i);
				}
			}
	
			image.setImageMain(true);
		}
		
		imageService.saveImage(image);
		
		return "redirect:/admin/product/" + image.getProduct().getProductId() + "/image/list";
	}
	
	@GetMapping("/admin/product/{productId}/image/{imageId}/delete")
	public String deleteImageProduct(@PathVariable("imageId") int imageId, 
									@PathVariable("productId") int productId, 
									RedirectAttributes redirect) {

		imageService.deleteImage(imageId);
		redirect.addFlashAttribute("success", "Đã xóa Hình ảnh thành công!");

		return "redirect:/admin/product/" + productId + "/image/list";
	}

	@GetMapping("/admin/product/image/{imageId}/edit")
	public String editImageProduct(@PathVariable("imageId") int imageId, Model model) {

		model.addAttribute("image", imageService.getByImageId(imageId));

		return "admin/product-image-edit";
	}

	@PostMapping("/admin/product/image/edit/save")
	public String saveEditImageProduct(@ModelAttribute("image") Image image, @RequestParam("file") MultipartFile file) {

		Image currentImage = imageService.getByImageId(image.getImageId());

		if (file != null && !file.getOriginalFilename().isEmpty()) {
			currentImage.setImageName(imageService.storeFile(file));
		}

		currentImage.setImageStatus(image.isImageStatus());

		// Update main of Products
		if (image.isImageMain()) {

			Product product = productService.getByProductId(image.getProduct().getProductId());

			for (Image i : product.getImages()) {
				i.setImageMain(false);
				imageService.saveImage(i);
			}

			currentImage.setImageMain(true);
		}

		imageService.saveImage(currentImage);

		return "redirect:/admin/product/" + image.getProduct().getProductId() + "/image/list";
	}

}
