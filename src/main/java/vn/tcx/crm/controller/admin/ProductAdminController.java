package vn.tcx.crm.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import vn.tcx.crm.entity.Product;
import vn.tcx.crm.service.CategoryService;
import vn.tcx.crm.service.ManufacturerService;
import vn.tcx.crm.service.ProductService;

@Controller
public class ProductAdminController {

	@Autowired
	private ProductService productService;
	
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private ManufacturerService manufacturerService;
	
	@GetMapping("/admin")
	public String index() {
		
		return "admin/admin-index";
	}
	
	@GetMapping("/admin/product/list")
	public String adminListProduct(Model model) {
		
		model.addAttribute("products", productService.findAllProductAdmin());
		
		return "admin/product-list";
	}
	
	@GetMapping("/admin/product/new")
	public String addNewProduct(Model model) {
		
		Product product = new Product();
		
		product.setProductStatus(true);
		
		model.addAttribute("product", product);
		model.addAttribute("categories", categoryService.findAllCategory());
		model.addAttribute("manufacturers", manufacturerService.findAllManufacturer());
		
		return "admin/product-addnew";
	}
	
	@PostMapping("/admin/product/save")
	public String addNewProduct(@ModelAttribute("product") Product product, BindingResult result, RedirectAttributes redirect) {
		
		productService.saveProduct(product);
		
		return "redirect:/admin/product/list";
	}
	
	@GetMapping("/admin/product/edit/{productId}")
	public String editProduct(@PathVariable(value = "productId") int productId, Model model) {
		
		model.addAttribute("product", productService.getByProductId(productId));
		model.addAttribute("categories", categoryService.findAllCategory());
		model.addAttribute("manufacturers", manufacturerService.findAllManufacturer());
		
		return "admin/product-edit";
	}
	
	@GetMapping("/admin/product/delete/{productId}")
	public String deleteProduct(@PathVariable(value = "productId") int productId, RedirectAttributes redirect) {
		
		Product product = productService.getByProductId(productId);
		
		productService.deleteProuct(product);
		
		redirect.addFlashAttribute("success", "Đã xóa sản phẩm thành công!");
		
		return "redirect:/admin/product/list";
	}
}
