package vn.tcx.crm.service;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.ResponseEntity;

import vn.tcx.crm.dto.ProductDTO;
import vn.tcx.crm.dto.ProductImageDTO;
import vn.tcx.crm.entity.Product;

public interface ProductService {
	List<Product> findAllProductAdmin();
	
	List<ProductImageDTO> findAllProduct();

	Product getByProductId(int productId);
	
	ProductImageDTO getByProductImageIdDTO(int productId);

	List<ProductImageDTO> lastestProducts(boolean productStatus, boolean productFeatured, int n);

	List<ProductImageDTO> featuredProducts(boolean productStatus, int index);

	long countByProductCategory(Product productCategory);

	List<Product> searchProductByProductName(String productName);

	Product saveProduct(Product product);

	void deleteProuct(Product product);

	Optional<Product> findProductById(int productId);

	// API

	List<Product> findByCategoryId(int categoryId);

	List<Product> findByManufacturerId(int manufacturerId);

	boolean existsByProductId(int productId);

	Product createProduct(ProductDTO productDTO);

	Product updateProduct(int productId, ProductDTO productDTO);

	//Export Excel, CSV, Word
	
	ResponseEntity<byte[]> exportExcel(List<Product> products);

	void exportCSV(HttpServletResponse response);
	
	ResponseEntity<byte[]> exportWord() throws Exception;
	
}
