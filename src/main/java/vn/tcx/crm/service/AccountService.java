package vn.tcx.crm.service;

import vn.tcx.crm.entity.Account;

public interface AccountService {
	
//	boolean checkLogin(Account account);

	Iterable<Account> getAllAccount();
	
	Account getAccountId(int accountId);
	
	void saveAccount(Account account);
	
	void deleteAccount(Account account);
	
//	List<User> findByNameContaining(String s);
	
//	Account findUsernameAndPassword(String username, String password);
}
