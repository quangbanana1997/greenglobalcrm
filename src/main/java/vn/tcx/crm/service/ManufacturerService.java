package vn.tcx.crm.service;

import java.util.List;
import java.util.Optional;

import vn.tcx.crm.entity.Manufacturer;

public interface ManufacturerService {

	List<Manufacturer> findAllManufacturer();
	
	Manufacturer getManufacturerById(int manufacturerId);
	
	Optional<Manufacturer> findManufacturerById(int manufacturerId);
	
	Manufacturer saveManufacturer(Manufacturer manufacturer);
	
	void deleteManufacturer(Manufacturer manufacturer);
	
	boolean existsManufacturerById(int manufacturerId);
	
	Manufacturer updateManufacturer(int manufacturerId, Manufacturer manufacturerUpdated);
}
