package vn.tcx.crm.service.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import vn.tcx.crm.dto.ProductDTO;
import vn.tcx.crm.entity.Product;

@Mapper
public interface ProductMapper {
	
	ProductMapper INSTANCE = Mappers.getMapper( ProductMapper.class );
	
	@Mapping(source = "category.categoryId", target = "categoryId")
	@Mapping(source = "manufacturer.manufacturerId", target = "manufacturerId")
	ProductDTO productToProductDTO(Product product);
	
	@Mapping(source = "categoryId", target = "category.categoryId")
	@Mapping(source = "manufacturerId", target = "manufacturer.manufacturerId")
	Product productDTOToProduct(ProductDTO productDTO);
	
	List<ProductDTO> productsToProductDTOs(List<Product> products);
}
