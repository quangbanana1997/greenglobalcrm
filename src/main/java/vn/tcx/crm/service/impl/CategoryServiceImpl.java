package vn.tcx.crm.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.tcx.crm.entity.Category;
import vn.tcx.crm.exception.NotFoundException;
import vn.tcx.crm.repository.CategoryRepository;
import vn.tcx.crm.service.CategoryService;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService{

	@Autowired
	private CategoryRepository categoryRepository;
	
	@Override
	public List<Category> findAllCategory() {
		return categoryRepository.findAll();
	}

	@Override
	public Category getCategoryById(int categoryId) {
		return categoryRepository.getOne(categoryId);
	}
	
	@Override
	public Optional<Category> findCategoryById(int categoryId) {
		return categoryRepository.findById(categoryId);
	}

	@Override
	public Category saveCategory(Category category) {
		return categoryRepository.save(category);
	}

	@Override
	public void deleteCategory(Category category) {
		categoryRepository.delete(category);
	}

	@Override
	public boolean existsCategoryById(int categoryId) {
		return categoryRepository.existsById(categoryId);
	}

	@Override
	public Category updateCategory(int categoryId, Category categoryUpdated) {
		
		Category category = categoryRepository.findById(categoryId)
				.orElseThrow(() -> new NotFoundException("Category not found with id " + categoryId));
		
		category.setCategoryName(categoryUpdated.getCategoryName());
		
		return categoryRepository.save(category);
	}

}