package vn.tcx.crm.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.tcx.crm.entity.Account;
import vn.tcx.crm.repository.AccountRepository;
import vn.tcx.crm.service.AccountService;

@Service
@Transactional
public class AccountServiceImpl implements AccountService{

	@Autowired 
	private AccountRepository accountRepository;
	
	
//	@Override
//	public boolean checkLogin(Account account) {
//		account = accountRepository.findByUsernameAndPassword(account.getUsername(), account.getPassword());
//		return account != null;
//	}
	
	@Override
	public Iterable<Account> getAllAccount() {
		return accountRepository.findAll();
	}

	@Override
	public Account getAccountId(int accountId) {
		return accountRepository.getOne(accountId);
	}

	@Override
	public void saveAccount(Account account) {
		accountRepository.save(account);
	}

	@Override
	public void deleteAccount(Account account) {
		accountRepository.delete(account);
	}

//	@Override
//	public List<User> findByNameContaining(String s) {
//		return userRepository.findByNameContaining(s);
//	}

//	@Override
//	public Account findUsernameAndPassword(String username, String password) {
//		return accountRepository.findByUsernameAndPassword(username, password);
//	}

}
