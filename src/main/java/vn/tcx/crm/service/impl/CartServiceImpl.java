package vn.tcx.crm.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import vn.tcx.crm.entity.Item;
import vn.tcx.crm.service.CartService;
import vn.tcx.crm.service.ProductService;

@Service
public class CartServiceImpl implements CartService{
	
	@Autowired
	private ProductService productService;
	
	@Override
	public void cart(HttpSession session) {
		
//		int countItems = 0;
//		double total = 0;
//		if(session.getAttribute("cart") != null) {
//			List<Item> cart = (List<Item>) session.getAttribute("cart");
//			countItems = cart.size();
//			for(Item item : cart) {
//				total += item.getProduct().getProductPrice() * item.getProductQuantity();
//			}
//		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public String addToCart(int productId, HttpSession session) {
		
		if(session.getAttribute("cart") == null) {
			List<Item> cart = new ArrayList<>();
			cart.add(new Item(productService.getByProductId(productId), 1));
			session.setAttribute("cart", cart);
		} else {
			List<Item> cart = (List<Item>) session.getAttribute("cart");
			int index = isExisting(productId, cart);
			if (index == -1) {
				cart.add(new Item(productService.getByProductId(productId), 1));
			} else {
				int newQuantity = cart.get(index).getProductQuantity() +1;
				cart.get(index).setProductQuantity(newQuantity);
			}
			session.setAttribute("cart", cart);
		}
		return "redirect:/cart";
	}

	@Override
	public int isExisting(int productId, List<Item> cart) {

		for(int i = 0; i < cart.size(); i++) {
			if(cart.get(i).getProduct().getProductId() == productId) {
				return i;
			}
		}
		return -1;
	}

}
