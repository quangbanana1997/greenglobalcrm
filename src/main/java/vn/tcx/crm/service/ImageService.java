package vn.tcx.crm.service;

import java.util.List;
import java.util.Optional;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import vn.tcx.crm.entity.Image;

public interface ImageService {

	Image getByImageId(int imageId);

	List<Image> findAllImages();
	
	Optional<Image> findImageById(int imageId);
	
	void deleteImage(Image image);
	
	List<Image> findByProductId(int productId);
	
	String storeFile(MultipartFile file);
	
	Resource loadFileAsResource(String fileName);
	
	Image getImage(int imageId);

	Image saveImage(Image image);

	void deleteImage(int imageId);
	
	Image createImage(int productId, Image image);
	
	Image upadateImage(int productId, int imageId, Image imageUpdated);
}
