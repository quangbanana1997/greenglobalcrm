package vn.tcx.crm.service;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.PathVariable;

import vn.tcx.crm.entity.Item;

public interface CartService {
	
	void cart(HttpSession session);

	String addToCart(@PathVariable(value = "productId") int productId, HttpSession session);
	
	int isExisting(int productId, List<Item> cart);
}
